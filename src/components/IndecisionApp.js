import React from "react"

import AddOption from "./AddOption"
import Header from "./Header"
import Action from "./Action"
import Options from "./Options"
import OptionModal from "./OptionModal"

export default class IndecisionApp extends React.Component {

  state = {
    title: "Indecision",
    subtitle: "Put your life in the hands of a computer",
    options: [],
    selectedOption: undefined
  }

  handleDeleteOptions = () => {
    this.setState(() => ({ options: [] }))
  }

  handleDeleteOption = option => {
    console.log("hdo", option)
    this.setState((prevState) => ({options: prevState.options.filter((el) => el !== option
    )}))
  }

  handlePick = () => {
    const randomNum = Math.floor(Math.random() * this.state.options.length)
    const option = this.state.options[randomNum]
    this.setState(()=>  ({
      selectedOption: option
    }))
  }

  handleClearSelectedOption = () => {
    this.setState(() => ({ selectedOption: undefined }))
  }

  handleAddOption = option => {
    if (!option) {
      return 'Enter valid value to add item'
    }
    else if (this.state.options.indexOf(option) > -1) {
      return 'This option already exists'
    }
    console.log(this.state.options)
    this.setState((prevState) =>({ options: prevState.options.concat(option) }))
  }

  componentDidMount() {
    try {
      const json = localStorage.getItem("options")
      const options = JSON.parse(json)
      if (options) {
        this.setState(()=>({ options }))
      }
    } catch (error) {
      //Do nothing at all
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if ( prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options)
      localStorage.setItem("options", json)
    }
  }

  componentWillUnmount() {
    console.log("component will unmount")
  }

  render(){
    return (
      <div>
        <Header title={this.state.title} subtitle={this.state.subtitle} />
        <div className="container">
          <Action
            hasOptions={this.state.options.length}
            handlePick={this.handlePick}
          />
          <div className="widget">
            <Options
              options={this.state.options}
              handleDeleteOptions={this.handleDeleteOptions}
              handleDeleteOption={this.handleDeleteOption}
            />
            <AddOption
              handleAddOption={this.handleAddOption}
            />
          </div>
        </div>
        <OptionModal
          selectedOption={this.state.selectedOption}
          handleClearSelectedOption={this.handleClearSelectedOption}
        />
      </div>
    )
  }
}