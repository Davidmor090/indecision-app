console.log("app.js is running")

const app = {
  title: "INDECISION APP",
  subtitle: "app info",
  options: []
  
}

const onFormSubmit = (e) =>{
  e.preventDefault()
  console.log("form submitted")

  const option = e.target.elements.option.value

  if (option) {
    app.options.push(option)
    e.target.elements.option.value = ""
  }

  render()
}

const emptyList = () =>{
  app.options = []
  render()
}

const appRoot = document.getElementById("app")

const render = () =>{
  const template = (
    <div>
      <h1>{app.title}</h1>
      {app.subtitle && <p>{app.subtitle}</p>}
      <p>{app.options.length > 0 ? "Here are your options" : "No Options"}</p>
      <p>{app.options.length}</p>
      <button onClick={emptyList}>Remove All</button>
      <ol>
        {
          app.options.map((option, index)=>{
            return <li key={index}>{option}</li>
          })
        }
      </ol>
      <form onSubmit={onFormSubmit}>
        <input type="text" name="option"/>
        <button>Add Option</button>
      </form>
    </div>
    )
    ReactDOM.render(template, appRoot)
}

render()
