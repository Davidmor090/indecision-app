class VisibilityToggle extends React.Component {
  
  constructor(props){
    super(props)
    this.handleToggleVisibility = this.handleToggleVisibility.bind(this)

    this.state = {
      title: "Visibility Toggle",
      visibility: false
    }
  }


  handleToggleVisibility() {
    this.setState((prevState) => {
      return { visibility: !prevState.visibility }
    })
  }


  render() {
    return (
      <div>
        <h1>{this.state.title}</h1>
        <button onClick={this.handleToggleVisibility}>{!this.state.visibility ? "Show details" : "Hide details"}</button>
        {this.state.visibility && 
        <div>
          <p>Hey, these are some details you can now see!</p>
        </div>}
      </div>
    )
  }

}

ReactDOM.render(<VisibilityToggle />, document.getElementById("app"))