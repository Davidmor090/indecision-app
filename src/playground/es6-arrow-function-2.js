const add = (a,b) => {
    // console.log(arguments)
    return a + b;
}

console.log(add(55,1))

const user = {
    name: "Andrew",
    cities: ["Barcelona", "Madrid", "Sevilla"],

    printPlacesLived() {
        console.log(this.name)
        console.log(this.cities)

        return this.cities.map((city) => this.name + " has lived in " + city
        );
        
    }
}

console.log(user.printPlacesLived());

const multiplier = {
    numbers: [1, 2, 3],
    multiplier: 5,

    multiply() {
        return this.numbers.map((number)=> number * this.multiplier)
    }

}

console.log(multiplier.multiply())